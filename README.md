# PiPlayer WebUI

Only running on the controlpi (Pi0) which provides the web-ui to the user.

It POST/GET calls to the PiPlayer API in each one of the Pis in the network,
to upload files and set up the media player software.


## Run

`cd piplayerwebui`

`python -m piplayerwebui`

## TODO:
How to ensure that `def piplayer_api_hosts()` picks up all the api IPs even if the API start after the webui?
* perform regular executions of the functions
* control order of start up 

ensure that when 

