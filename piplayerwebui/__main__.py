from piplayerwebui.app import ui
from piplayerwebui.config import config_envs
from os import getenv


if __name__ == '__main__':
    env = getenv('PIPLAYERWEBUI_ENV', 'dev')
    print('Enviroment: {}'.format(env))
    conf = config_envs.get(env)
    ui.run(host=conf.host, port=conf.port, debug=conf.debug)
