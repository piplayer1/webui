import os


class Config:
    debug = False
    # all common values to 3 environemnts
    basedir = '/home/andre/Documents/Projects/PiPlayer/'
    logs_dir = os.path.join(basedir, 'logs/')
    webui_log = os.path.join(logs_dir, 'webui.log')
    allowed_formats = ['mp4', 'mov', 'avi', 'ogv', 'mp3', 'ogg', 'flac', 'wav']
    api_url_pi1 = 'http://0.0.0.0:5001'
    api_port = '5001'


class DevelopmentConfig(Config):
    debug = True
    host = '0.0.0.0'
    port = 5000
    player = 'mpv'


class testingConfig(Config):
    debug = True
    testing = True
    host = '0.0.0.0'
    port = 5000


class ProductionConfig(Config):
    debug = False
    host = None  # '0.0.0.0'
    port = None  # 5001
    player = 'oxm'
    # TODO: setting for gunicorn


config_envs = dict(
    dev=DevelopmentConfig,
    test=testingConfig,
    prd=ProductionConfig
)