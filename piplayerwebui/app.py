#!/usr/bin/env python3
import os
import sys
from flask import (Flask,
                   session,
                   request,
                   render_template,
                   redirect,
                   url_for)
from piplayerwebui.utils.player import Player
from piplayerwebui.config import config_envs
from piplayerwebui.utils.routefunctions import (query_api,
                                                handle_fileupload,
                                                handle_filedelete,
                                                piplayer_api_hosts)
from piplayerwebui.utils.log import logger


# from piplayerwebui.utils.log import (log, logger)

env = os.getenv('PIPLAYERAPI_ENV', 'dev')
conf = config_envs.get(env)

ui = Flask(__name__)
ui.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

logger.info(msg='Starting {}'.format(__name__))

hosts_running_api = piplayer_api_hosts(port=conf.api_port)
logger.info(msg='Hosts running api: {}'.format(hosts_running_api))

'''

# functions
def template_w_files(template: str, path: str, kargs: object={}) -> str:
    # get current list of files
    # and render the template using them
    pprint(kargs)
    files_in_dir = os.listdir(ui.config['UPLOAD_DIR'])
    return render_template(template,
                           userdir=ui.config['UPLOAD_DIR'],
                           files=files_in_dir,
                           path=path,
                           menulinks=menulinks,
                           kargs=kargs)
'''

'''


def storeNtest_player(action: str) -> str:
    if action == 'test':
        process = subprocess.Popen(player.cmd,
                                   shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        # stdout, stderr = process.communicate()
        # logger.info(stdout)
        # logger.error(stderr)

        msg = 'Playing'
    elif action == 'store':
        try:
            script = ui.config['PLAYER_SCRIPT']
            with open(script, 'w') as PLAYER_SCRIPT:
                PLAYER_SCRIPT.write('#!/bin/sh\n{}'.format(player.cmd))
            os.chmod(script, 0o777)
            msg = '<p>Settings saved successfully to: <code>{}<code>\
            </p><p>Full command: \
            <code>{}</code></p>'.format(script, player.cmd)
        except Exception as e:
            msg = 'Failed to save settings'
        # TODO: add to ui.config['PLAYER_SCRIPT'] .xinitrc
    else:
        msg = 'Missing msg'
    # print(msg, action)
    return msg
'''



# ROUTES

@ui.route('/addhost')
def addhost():
    '''
    to be called by each piplayerapi instance when it starts
    so that piplayerwebui becomes aware of its existance
    if piplayerapi instance starts after piplayerwebui has run piplayer_api_hosts
    '''
    addr = request.environ['REMOTE_ADDR']
    port = str(request.environ['REMOTE_PORT'])
    if port == conf.api_port and url not in hosts_running_api and url is not '127.0.0.1':
        hosts_running_api.append(addr)
        logger.info(msg=f'Added host: {addr} to {hosts_running_api}')
    return redirect(url_for('main'))


@ui.route('/')
def main():
    return render_template('main.html', path=request.path, menulinks=menulinks)


@ui.route('/upload', methods=['GET', 'POST'])  # upload form post handler
def upload():
    if request.method == 'GET':
        # Get Files__name__
        query_url = conf.api_url_pi1 + '/files'
        resp_code, resp_json = query_api(url=query_url)
        _files = [k for k in resp_json.keys()]
        # Get hostname of API's Pi
        apiinfo_code, apiinfo_json = query_api(url=conf.api_url_pi1)
        hostname = apiinfo_json.get('info').get('hostname')
        return render_template('upload.html',
                               currentpath=request.path,
                               menulinks=menulinks,
                               ipaddr=conf.api_url_pi1,
                               hostname=hostname,
                               files=_files,
                               ui_msg=request.args.get('ui_msg'),
                               apiresp=request.args.get('apiresp'),
                               apirespcode=request.args.get('apirespcode'))
    elif request.method == 'POST':
        form = request.args.get('form')
        if form == 'upload':
            file = request.files['fileinput']  # uploading files
            ui_msg, apiresp, apirespcode = handle_fileupload(request, file)

        elif form == 'delete':
            todelete = request.form.getlist('todelete')
            ui_msg, apiresp, apirespcode = handle_filedelete(todelete)

        # print(form, ui_msg)
        return redirect(url_for(request.path[1:],
                                ui_msg=ui_msg,
                                apiresp=apiresp,
                                apirespcode=apirespcode))

'''


@ui.route('/settings', methods=['GET', 'POST'])
def settings():
    if request.method == 'GET':
        return template_w_files('settings.html',
                                request.path,
                                kargs=request.args)

    elif request.method == 'POST':
        if request.form.get('debug'):
            logfile = ui.config['PLAYER_LOG']
        else:
            logfile = None
        print('DEBUG VALUES', request.form.get('debug'), logfile)

        player.player_set(file=os.path.join(ui.config['UPLOAD_DIR'],
                                            request.form.getlist('file')[0]),
                          loop=request.form.get('loop'),
                          volume=request.form.get('volume'),
                          logfile=logfile,
                          )

        msg = storeNtest_player(action=request.form.get('submit'))
        logger.info(msg=player.__dict__)
        return redirect(url_for('settings', msg=msg, **player.__dict__))
'''

with ui.test_request_context():
    menulinks = {
        'main page': url_for('main'),
        'upload/remove media': url_for('upload'),
        # 'player settings': url_for('settings')
    }


@ui.route('/version')
def version():
    version = "Python version: {}".format(sys.version)
    return version

if __name__ == '__main__':
    ui.run(debug=True)
