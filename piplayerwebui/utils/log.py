import logging
import os
import json
from logging.handlers import RotatingFileHandler
from piplayerwebui.config import config_envs


def log(log_path):
    logger = logging.getLogger('PiPlayer API Log')
    log_format = logging.Formatter(

        json.dumps({'date': '%(asctime)s',
                    'name': '%(name)s',
                    'level': '%(levelname)s',
                    'msg': '%(message)s',
                    })
    )

    log_handler = RotatingFileHandler(filename=log_path,
                                      maxBytes=512000,
                                      backupCount=2)  # 512Kb log file
    log_handler.setFormatter(log_format)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)
    return logger


# if __name__ == '__main__':
env = os.getenv('PIPLAYERWEBUI_ENV', 'dev')
conf = config_envs.get(env)
logger = log(conf.webui_log)  # create logger
