class Player:
    """Player class 
    * store and processes the parametes of the players in use
    * produces and stores the player invocation command
    """
    def __init__(self, player):
        self.player = player
        self.loop = None
        self.loop_html_value = None
        self.volume = None
        self.volume_html_value = None
        self.cursorhide = None
        self.file = None
        self.file_html_value = None
        self.logfile = None
        self.loglevel = None
        # .*_html_value is the str used by the settings.html
        self.player_set()

    def settings2cmd(self):
        # turn settings on a sh cmd string
        # filters out keys:
        # * 'cmd' to avoid recursion
        # * '.*_html_value' input values
        # * and ones with empty values
        # reorder according to each player
        if self.player == 'mpv':
            # filter
            cmd = [v for k, v in self.__dict__.items() if
                   'html_value' not in k and k not in ['cmd'] and
                   v is not None]
            # reorder
            cmd.remove(self.player)  # player at start
            cmd.insert(0, self.player)
            cmd.remove(self.file)  # file at end
            cmd.append(self.file)
            if self.loglevel:
                cmd.remove(self.loglevel)  # file at end
                cmd.insert(1, self.loglevel)
            # make script
            self.cmd = (" ").join(cmd)
        elif self.player == 'omx':
            pass

    def player_set(self, loop=None, volume=None, file=None, logfile=None):
        if self.player == 'mpv':
            self.fullscreen = '--fs'
            self.cursorhide = '--cursor-autohide=always'
            self.noterminal = '--no-terminal' 
            if loop == 'loop':
                self.loop = '--loop-file=inf'
                self.loop_html_value = 'loop'
            else:
                self.loop = '--loop-file=no'
                self.loop_html_value = None
            if volume:
                self.volume = '--volume=' + str(volume)
                self.volume_html_value = str(volume)
            if file:
                self.file = file
                self.file_html_value = (file.split('/'))[-1]
            if logfile:
                self.logfile = '--log-file={}'.format(logfile)
                self.loglevel = '--msg-level=all=v'
            else:
                self.logfile = self.loglevel = None

        elif self.player == 'mplayer':
            self.fullscreen = '-fs'
            if loop is True:
                self.loop = '-loop 0'
            elif loop is False:
                self.loop = '-loop 1'

        if self.player and self.file:
            # every time a setting is changed
            # update cmd string
            self.settings2cmd()


if __name__ == '__main__':
    p = Player(player='mpv')
    p.player_set(file='foo.mp4')
    p.player_set(loop=True)
    print(p.__dict__)
    print(p.loop)
    p.player_set(loop=False)
    print(p.loop)
    p.player_set(volume=100)
    print(p.volume)
    p.settings2cmd()
    print('cmd:', p.cmd)

    p.player_set(volume=10)
    p.player_set(loop=True)
    # mpv.settings2cmd()
    print('cmd:', p.cmd)
