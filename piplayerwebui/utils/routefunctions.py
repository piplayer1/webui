import socket
import subprocess
import re
import requests
from piplayerwebui.utils.log import logger


# TODO:
# * check file mimetype: allowed
# * file size before it is passed to api and after: compare tehe 2


def query_api(url: str, method: str='GET', data=None) -> tuple:
    if method == 'GET':
        response = requests.get(url)
    elif method == 'POST' and data:
        response = requests.post(url, data=data)
    logger.debug(msg="Call: {} {}".format(method, url))
    resp_code = response.status_code
    resp_json = response.json()
    logger.debug(msg="Response: {} {}".format(resp_json, resp_code))
    return resp_code, resp_json


def handle_fileupload(rq: object,
                      _file: object,
                      keeptmp=False,
                      addr='http://0.0.0.0:5001') -> str:

    if 'fileinput' not in rq.files.keys() or _file.filename == '':
        return 'No files selected for upload'
    fileinput = rq.files['fileinput']
    mimetype = fileinput.mimetype
    upload_url = '{}/upload/{}'.format(addr, fileinput.filename)
    files_url = '{}/files/'.format(addr)
    resp_code, resp_json = query_api(url=upload_url,
                                     method='POST',
                                     data=fileinput.read())

    # if fileinput:  # TODO: check formats w/ mimetype
    #     try:
    #         with TempFile(prefix='upload_', delete=keeptmp) as tmpupload:
    #             tmpupload.write(fileinput.read())
    #             fn = tmpupload.name

    if resp_code is 200:
        ui_msg = 'File {} successfully uploaded to {}'.format(
            fileinput.filename,
            files_url)
        resp_msg = "File: {} - {}".format(resp_json[0].get('file'),
                                          resp_json[0].get('uploaded'))
        return ui_msg, resp_msg, resp_code
    else:
        ui_msg = 'File {} FAILED to uploaded to {}'.format(
            fileinput.filename,
            files_url)
        return ui_msg, resp_json.get('error'), resp_code


def handle_filedelete(f2delete: str,
                      addr: str='http://0.0.0.0:5001') -> str:
    deleted_files = []
    for _f in f2delete:
        url_delete = '{}/delete/{}'.format(addr, _f)
        resp_code, resp_json = query_api(url=url_delete)
        if resp_code is 200:
            deleted_files.append(_f)
    if len(deleted_files) > 0:
        df_str = (" ").join(deleted_files)
        ui_msg = 'Deleted Files: {}'.format(df_str)
        resp_code = 200
    else:
        ui_msg = 'No files were deleted'
        resp_code = 400
    return ui_msg, '', resp_code


def piplayer_api_hosts(port:str) -> list:

    # find own ip addres
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('8.8.8.8', 1))
    except:
        # network in unreachable
        # using localhost only localhost
        return ['127.0.0.1']
    webui_ip_address = s.getsockname()[0]
    # scan machines in same LAN using nmpa
    nmap = 'nmap -sP ' + ('.'.join(webui_ip_address.split('.')[:3])) + '.0/24'
    process = subprocess.Popen(nmap,
                               shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    nmap_response = [line for line in stdout.decode('utf-8').split('\n') if line.startswith('Nmap scan report for')]
    host_ips = [ (re.findall(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', response))[0] for response in nmap_response]
    ips = []
    for host in host_ips:
        try:
            response = requests.get(f'http://{host}:{port}') # use setting here to determine port
            if response.status_code is 200:
                ips.append(host)
        except:
            pass
    return ips

